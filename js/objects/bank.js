let bankClients = [{
    name: 'Alex',
    surname: 'Alexis',
    activeClients: false,
    registrationDate: '12-09-2020',
    check: {
        debit: {
            balance: 20000,
            dateIssue: '12-09-2020',
            endTerm: '12-09-2025',
            currency: 'USD',
            active: true,
        },
        credit: {
            balance: 5000,
            creditLimit: 10000,
            dateIssue: '12-09-2021',
            endTerm: '12-09-2026',
            currency: 'RUR',
            active: false,
        },
    }
},
{
    name: 'Aleksandr',
    surname: 'Hoggarth',
    activeClients: true,
    registrationDate: '20-01-2015',
    check: {
        debit: {
            balance: 0,
            dateIssue: '25-01-2015',
            endTerm: '25-01-2020',
            currency: 'RUR',
            active: false,
        },
        credit: {
            balance: 5000,
            creditLimit: 10000,
            dateIssue: '25-05-2015',
            endTerm: '25-05-2020',
            currency: 'USD',
            active: true,
        },
    }
},
{
    name: 'Kirill',
    surname: 'Nash',
    activeClients: false,
    registrationDate: '20-01-2010',
    check: {
        debit: {
            balance: 0,
            dateIssue: '25-01-2011',
            endTerm: '25-01-2016',
            currency: 'EUR',
            active: false,
        },
        credit: {
            balance: 12000,
            creditLimit: 20000,
            dateIssue: '25-05-2011',
            endTerm: '25-05-2016',
            currency: 'USD',
            active: true,
        },
    }
},
{
    name: 'Aleksey',
    surname: 'Holiday',
    activeClients: true,
    registrationDate: '20-01-2021',
    check: {
        debit: {
            balance: 50000,
            dateIssue: '25-01-2021',
            endTerm: '25-01-2026',
            currency: 'UAH',
            active: true,
        },
        credit: {
            balance: 24000,
            creditLimit: 30000,
            dateIssue: '25-05-2021',
            endTerm: '25-05-2026',
            currency: 'EUR',
            active: true,
        },
    }
},
{
    name: 'Anatoly',
    surname: 'James',
    activeClients: true,
    registrationDate: '20-01-2019',
    check: {
        debit: {
            balance: 10000,
            dateIssue: '25-01-2019',
            endTerm: '25-01-2024',
            currency: 'UAH',
            active: true,
        },
        credit: {
            balance: 5000,
            creditLimit: 5000,
            dateIssue: '25-05-2019',
            endTerm: '25-05-2024',
            currency: 'EUR',
            active: true,
        },
    }
},
]

class Bank {
    constructor(param) {
        this.bankClients = param;
    }

    serviceRequest(param, callback) {
        let url = `https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5`;
        return fetch(url).then(data => data.json()).then(mass => {
            let currency = {};
            for(let index in mass){
                currency[mass[index].ccy] = mass[index].buy;
            }

            return callback(param, currency);
        })
    }

    totalAmountMoney() {
        let param = {};

        this.bankClients.forEach(item => {
            param[item.check.debit.currency] = param[item.check.debit.currency] || 0;
            param[item.check.credit.currency] = param[item.check.credit.currency] || 0;

            param[item.check.debit.currency] += item.check.debit.balance;
            param[item.check.credit.currency] += item.check.credit.balance;
        });

        return this.serviceRequest(param, (param, currency) => {
            let result = param.uah || 0;
            for(let i in currency) {
                for(let j in param){
                    if(i === j){
                        result += param[j] * currency[i];
                    } 
                }
            }

            return result;
        });
    }

    oweBank() {
        let param = {};

        this.bankClients.forEach(item => {
            if (item.check.credit.creditLimit >= item.check.credit.balance) {
                param[item.check.credit.currency] = param[item.check.credit.currency] || 0;
                param[item.check.credit.currency] += item.check.credit.creditLimit - item.check.credit.balance;
            }
        });

        return this.serviceRequest(param, (param, currency) => {
            let result = param.uah || 0;
            for(let i in currency) {
                for(let j in param){
                    if(i === j){
                        result += param[j] * currency[i];
                    } 
                }
            }

            return result;
        });
    }

    CustomersOweBank(argument) {
        let param = {};

        this.bankClients.forEach(item => {
            if (item.check.credit.creditLimit >= item.check.credit.balance && item.activeClients === argument) {
                param[item.check.credit.currency] = param[item.check.credit.currency] || 0;
                param[item.check.credit.currency] += item.check.credit.creditLimit - item.check.credit.balance;
            }
        });

        return this.serviceRequest(param, (param, currency) => {
            let result = param.uah || 0;
            for(let i in currency) {
                for(let j in param){
                    if(i === j){
                        result += param[j] * currency[i];
                    } 
                }
            }

            return result;
        });
    }
}

const bank = new Bank(bankClients);

class RenderCards{
    constructor(){
        this.container = document.querySelector('.container');
        this.modal = document.querySelector('.modal');
        this.trueAdnFalse = null;
        this.renderUsers();
    }

    createCard(item, index){
        const cart = document.createElement('div');
        cart.className = 'usersContainer';
        cart.innerHTML = `
            <p>name: ${item.name}</p>
            <p>surname: ${item.surname}</p>
            <p>active clients: ${item.activeClients}</p>
            <p>registration date: ${item.registrationDate}</p>
            <div class="userCheck">
                <div class="cartDebit">
                    <h3>debit</h3>
                    <p>balance: ${item.check.debit.balance}</p>
                    <p>date issue: ${item.check.debit.dateIssue}</p>
                    <p>end term: ${item.check.debit.endTerm}</p>
                    <p>currency: ${item.check.debit.currency}</p>
                    <p>active: ${item.check.debit.active}</p>
                </div>
                <div class="cartCredit">
                    <h3>credit</h3>
                    <p>balance: ${item.check.credit.balance}</p>
                    <p>credit limit: ${item.check.credit.creditLimit}</p>
                    <p>date issue: ${item.check.credit.dateIssue}</p>
                    <p>end term: ${item.check.credit.endTerm}</p>
                    <p>currency: ${item.check.credit.currency}</p>
                    <p>active: ${item.check.credit.active}</p>
                </div>
            </div>
        `;

        const buttonChange = document.createElement('button');
        const buttonDelete = document.createElement('button');
        
        buttonChange.innerText = 'chenge';
        buttonDelete.innerText = 'delete';
        
        buttonDelete.addEventListener('click', this.deleteUsers.bind(this, index));
        buttonChange.addEventListener('click', this.changeUser.bind(this, item, index));

        cart.append(buttonChange, buttonDelete);
       
        return cart;
    }

    renderUsers(){
        this.container.innerHTML = '';
        const cards = bankClients.map((item, index) => this.createCard(item, index));
        this.container.append(...cards);
    }

    createModalUser(item, index){
        this.modal.innerHTML = '';
        const modalChange = document.createElement('form');
        modalChange.className = 'modalChange';
        modalChange.innerHTML = `
            <form>
                <span>name: <input name='name' placeholder = '${this.trueAdnFalse ? item.name : ''}' type="text"></span>
                <span>surname: <input name='surname' placeholder = '${this.trueAdnFalse ? item.surname : ''}' type="text"></span>
                <span>active clients:
                <select name='activeClients'>
                    <option value='${this.trueAdnFalse ? item.activeClients : ''}'>${this.trueAdnFalse ? item.activeClients : ''}</option>
                    <option value='false'>false</option>
                    <option value='true'>true</option>
                </select></span>
                <span>registration date: <input name='registrationDate' placeholder = '${this.trueAdnFalse ? item.registrationDate : ''}' type="date"></span>
                <div class="userCheck">
                    <div class="cartDebit">
                        <h3>debit</h3>
                        <span>balance: <input name='balanceDebit' placeholder = '${this.trueAdnFalse ? item.check.debit.balance : ''}' type="number"></span>
                        <span>date issue: <input name='dateIssueDebit' placeholder = '${this.trueAdnFalse ? item.check.debit.dateIssue : ''}' type="date"></span>
                        <span>end term: <input name='endTermDebit' placeholder = '${this.trueAdnFalse ? item.check.debit.endTerm : ''}' type="date"></span>
                        <span>currency: 
                        <select name='currencyDebit'>
                            <option value='${this.trueAdnFalse ? item.check.debit.currency : ''}'>${this.trueAdnFalse ? item.check.debit.currency : ''}</option>
                            <option value='RUB'>RUB</option>
                            <option value='USD'>USD</option>
                            <option value='EUR'>EUR</option>
                            <option value='UAH'>UAH</option>
                        </select></span>
                        <span>active: 
                        <select name='activeDebit'>
                            <option value='${this.trueAdnFalse ? item.check.debit.active : ''}'>${this.trueAdnFalse ? item.check.debit.active : ''}</option>
                            <option value='false'>false</option>
                            <option value='true'>true</option>
                        </select></span>
                    </div>
                    <div class="cartCredit">
                        <h3>credit</h3>
                        <span>balance: <input name='balanceCredit' placeholder = '${this.trueAdnFalse ? item.check.credit.balance : ''}' type="number"></span>
                        <span>credit limit: <input name='creditLimit' placeholder = '${this.trueAdnFalse ? item.check.credit.creditLimit : ''}' type="number"></span>
                        <span>date issue: <input name='nadateIssuemeCredit' placeholder = '${this.trueAdnFalse ? item.check.credit.dateIssue : ''}' type="date"></span>
                        <span>end term: <input name='endTermCredit' placeholder = '${this.trueAdnFalse ? item.check.credit.endTerm : ''}' type="date"></span>
                        <span>currency: 
                        <select name='currencyCredit'>
                            <option value='${this.trueAdnFalse ? item.check.credit.currency : ''}'>${this.trueAdnFalse ? item.check.credit.currency : ''}</option>
                            <option value='RUB'>RUB</option>
                            <option value='USD'>USD</option>
                            <option value='EUR'>EUR</option>
                            <option value='UAH'>UAH</option>
                        </select></span>
                        <span>active: 
                        <select name='activeCredit'>
                            <option value='${this.trueAdnFalse ? item.check.credit.active : ''}'>${this.trueAdnFalse ? item.check.credit.active : ''}</option>
                            <option value='false'>false</option>
                            <option value='true'>true</option>
                        </select>
                        </span>
                    </div>
                </div>
            </form>
        `;

        const buttonAdd = document.createElement('button');
        buttonAdd.innerText = 'Add';

        buttonAdd.addEventListener('click', this.chengeUserAndAddUser.bind(this, index));
        this.modal.addEventListener('click', this.closeModal.bind(this, this.modal));

        modalChange.append(buttonAdd);
        this.modal.append(modalChange);
    }
    
    onChengeUser(event, index){
        event.preventDefault();

        let data = new FormData(event.target.closest('form'));

        bankClients[index].name = data.get('name') || bankClients[index].name;
        bankClients[index].surname = data.get('surname') || bankClients[index].surname;
        bankClients[index].activeClients = data.get('activeClients') === 'true' ? true : false;
        bankClients[index].registrationDate = data.get('registrationDate') || bankClients[index].registrationDate;

        bankClients[index].check.debit.balance = Number(data.get('balanceDebit')) || bankClients[index].check.debit.balance;
        bankClients[index].check.debit.dateIssue = data.get('dateIssueDebit') || bankClients[index].check.debit.dateIssue;
        bankClients[index].check.debit.endTerm = data.get('endTermDebit') || bankClients[index].check.debit.endTerm;
        bankClients[index].check.debit.currency = data.get('currencyDebit') || bankClients[index].check.debit.currency;
        bankClients[index].check.debit.active = data.get('activeDebit') === 'true' ? true : false;

        bankClients[index].check.credit.balance = Number(data.get('balanceCredit')) || bankClients[index].check.credit.balance;
        bankClients[index].check.credit.creditLimit = data.get('creditLimit') || bankClients[index].check.credit.creditLimit;
        bankClients[index].check.credit.dateIssue = data.get('dateIssueCredit') || bankClients[index].check.credit.dateIssue;
        bankClients[index].check.credit.endTerm = data.get('endTermCredit') || bankClients[index].check.credit.endTerm;
        bankClients[index].check.credit.currency = data.get('currencyCredit') || bankClients[index].check.credit.currency;
        bankClients[index].check.credit.active = data.get('activeCredit') === 'true' ? true : false;
        
        this.modal.classList.remove('active');
        this.renderUsers();
    }

    addUser(event){
        event.preventDefault();

        let data = new FormData(event.target.closest('form'));

        bankClients.push({
            name: data.get('name') || null,
            surname: data.get('surname') || null,
            activeClients: data.get('activeClients') === 'true' ? true : false,
            registrationDate: data.get('registrationDate') || null,
            check: {
                debit: {
                    balance: Number(data.get('balanceDebit')) || null,
                    dateIssue: data.get('dateIssueDebit') || null,
                    endTerm: data.get('endTermDebit') || null,
                    currency: data.get('currencyDebit') || null,
                    active: data.get('activeDebit') === 'true' ? true : false,
                },
                credit: {
                    balance: Number(data.get('balanceCredit')) || null,
                    creditLimit: data.get('creditLimit') || null,
                    dateIssue: data.get('dateIssueCredit') || null,
                    endTerm: data.get('endTermCredit') || null,
                    currency: data.get('currencyCredit') || null,
                    active: data.get('activeCredit')  === 'true' ? true : false,
                },
            }
        });

        this.renderUsers();
        this.modal.classList.remove('active');
    }

    closeModal(param, event){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }

    chengeUserAndAddUser(index, event){
        if(this.trueAdnFalse === true){
            this.onChengeUser(event, index);
        } else if(this.trueAdnFalse === false){
            this.addUser(event);
        }
    }

    changeUser(item, index){
        this.modal.classList.add('active');
        this.trueAdnFalse = true;
        this.createModalUser(item, index);
    }

    deleteUsers (index){
        bankClients.splice(index, 1);
        this.renderUsers();
    }
}

const renderCards = new RenderCards();

class StatisticsBank{
    constructor(){
        this.statisticsModal = document.querySelector('.statisticsModal');
        this.totalAmountMoneytBlock = document.querySelector('.totalAmountMoneyResult');
        this.oweBankBlock = document.querySelector('.oweBankResult');
        this.customersOweBankResultBlock = document.querySelector('.customersOweBankResult');
        this.customersOweBankButtons = document.querySelector('.customersOweBankButtons');
    }

    statistics(){
        const totalAmountMoneyResult = bank.totalAmountMoney();
        const oweBankResult = bank.oweBank();
        const customersOweBankFalseResult = bank.CustomersOweBank(false);
        const customersOweBankTrueResult = bank.CustomersOweBank(true);

        totalAmountMoneyResult.innerText = '';
        oweBankResult.innerText = '';
        customersOweBankFalseResult.innerText = '';
        customersOweBankTrueResult.innerText = '';
        this.customersOweBankButtons.innerHTML = '';

        totalAmountMoneyResult.then(data => {
            this.totalAmountMoneytBlock.innerText = `${data}`;
        });
        oweBankResult.then(data => {
            this.oweBankBlock.innerText = `${data}`;
        });
        customersOweBankFalseResult.then(data => {
            this.customersOweBankResultBlock.innerText = `${data}`;
        });
        
        const activeBatton = document.createElement('button');
        const isActiveBatton = document.createElement('button');

        activeBatton.innerText = 'Active';
        isActiveBatton.innerText = 'No active';    

        activeBatton.addEventListener('click', this.statisticsActive.bind(this, customersOweBankFalseResult));
        isActiveBatton.addEventListener('click', this.statisticsActive.bind(this, customersOweBankTrueResult));
        this.statisticsModal.addEventListener('click', this.closeStatistics.bind(this, this.statisticsModal));

        this.customersOweBankButtons.append(activeBatton, isActiveBatton);
    }

    closeStatistics(param, event){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }

    statisticsActive(param){
        this.customersOweBankResultBlock.innerText = '';
        param.then(data => {
            this.customersOweBankResultBlock.innerText = `${data}`;
        })
    }
}

const statisticsBank = new StatisticsBank();

class Header extends RenderCards{
    constructor(){
        super();
        this.statisticsBank = statisticsBank;
        this.statisticsModal = document.querySelector('.statisticsModal');
        this.header = document.querySelector('.header');
        this.headerButtons();
    }
    
    headerButtons(){
        const statistics = document.createElement('button');
        const addUserButton = document.createElement('button');
        const restaurantLink = document.createElement('a');
        const bankLink = document.createElement('a');
        
        statistics.innerText = 'Statistics';
        addUserButton.innerText = 'Add user';
        restaurantLink.innerText = 'Restaurant';
        restaurantLink.href = './index.html';
        bankLink.innerText = 'Bank';
        bankLink.href = './bankHtml.html';

        addUserButton.addEventListener('click', this.addUserModal.bind(this));
        statistics.addEventListener('click', this.onStatistics.bind(this));
        
        return this.header.append(statistics, addUserButton, restaurantLink, bankLink);
    }
    
    onStatistics = () => {
        this.statisticsBank.statistics();
        this.statisticsModal.classList.add('active');
    }

    addUserModal(){
        this.modal.classList.add('active');
        this.trueAdnFalse = false;
        this.createModalUser();
    }
}

const header = new Header();
