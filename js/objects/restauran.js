let departmentNumber = {
    hall: 1,
    kitchen: 2,
    warehouse: 3,
}

let positionNumber = {
    Head: 1,
    bartender: 2,
    chef: 3,
    loader: 4,
    sorter: 5,
    waiter: 6,
    сook: 7,
}

let employeeObjectss = [
    {
        name: 'Alex',
        surname: 'Alexis',
        position: 'waiter',
        positionNumber: 6,
        salary: 1200,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Aleksandr',
        surname: 'Hoggarth',
        position: 'bartender',
        positionNumber: 2,
        salary: 1500,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Kirill',
        surname: 'Nash',
        position: 'bartender',
        positionNumber: 2,
        salary: 1600,
        works: false,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Aleksey',
        surname: 'Holiday',
        position: 'waiter',
        positionNumber: 6,
        salary: 1100,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    },
    {
        name: 'Anatoly',
        surname: 'James',
        position: 'Head',
        positionNumber: 1,
        salary: 2100,
        works: true,
        departmentNumber: 1,
        departmentName: 'hall',
    }, 
    {
        name: 'Aleksey',
        surname: 'Keat',
        position: 'waiter',
        positionNumber: 6,
        salary: 1100,
        works: false,
        departmentNumber: 1,
        departmentName: 'hall',
    },
    {
        name: 'Artur',
        surname: 'Kendal',
        position: 'Head',
        positionNumber: 1,
        salary: 2400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    },
    {
        name: 'Artem',
        surname: 'Kelly',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Boris',
        surname: 'Kennedy',
        position: 'сook',
        positionNumber: 7,
        salary: 1450,
        works: false,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Vadim',
        surname: 'Kennett',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Valentin',
        surname: 'Kingsman',
        position: 'сook',
        positionNumber: 7,
        salary: 1300,
        works: false,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Valeriy',
        surname: 'Kirk',
        position: 'chef',
        positionNumber: 3,
        salary: 1800,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Mikhail',
        surname: 'Nevill',
        position: 'chef',
        positionNumber: 3,
        salary: 2100,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Vasily',
        surname: 'Laird',
        position: 'сook',
        positionNumber: 7,
        salary: 1400,
        works: true,
        departmentNumber: 2,
        departmentName: 'kitchen',
    }, 
    {
        name: 'Viktor',
        surname: 'Lamberts',
        position: 'sorter',
        positionNumber: 5,
        salary: 1000,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Vitaly',
        surname: 'Larkins',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Vladimir',
        surname: 'Lawman',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Gleb',
        surname: 'Leman',
        position: 'sorter',
        positionNumber: 5,
        salary: 900,
        works: false,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Grigory',
        surname: 'Macey',
        position: 'sorter',
        positionNumber: 5,
        salary: 1000,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Daniil',
        surname: 'Mason',
        position: 'loader',
        positionNumber: 4,
        salary: 950,
        works: true,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
    {
        name: 'Maksim',
        surname: 'Oldman',
        position: 'loader',
        positionNumber: 4,
        salary: 1050,
        works: false,
        departmentNumber: 3,
        departmentName: 'warehouse',
    }, 
];

class Restaurant {
    constructor(param){
        this.employeeObjects = param;
    }

    sumSalariesDepartment(){
        let sum = {};

        this.employeeObjects.map((item) => {
            sum[item.departmentName] = sum[item.departmentName] || 0;
            sum[item.departmentName] += item.salary;
        });

        return sum;
    }

    averageSalaryDepartment(){
        let meanSalary = {};

        this.employeeObjects.map((item) => {
            meanSalary[item.departmentName] = meanSalary[item.departmentName] || [];

            for(let index in meanSalary){
                if(index === item.departmentName){
                    meanSalary[index].push(item.salary);
                }
            }
        });

        function average(name, items) {
            let sum = 0;

            for (let i = 0; i < items.length; i++) {
              sum += items[i];
            }

            meanSalary[name] = Math.floor(sum / items.length);
        }
        
        for(let index in meanSalary){
            average(index, meanSalary[index]);
        }

        return meanSalary;
    }

    salarysDepartment(colback){
        let departmentName = {};

        this.employeeObjects.map((item) => {
            departmentName[item.departmentName] = departmentName[item.departmentName] || [];
            for(let index in departmentName){
                if(index === item.departmentName){
                    departmentName[index].push(item.salary);
                }
            }
        });

        for(let index in departmentName){
            departmentName[index].sort(colback);
            departmentName[index] = departmentName[index][0];
        }

        return departmentName;
    }
        
    salarysPosition(colback){
        let position = {};
        this.employeeObjects.map((item) => {
            position[item.position] = position[item.position] || [];
            for(let index in position){
                if(index === item.position){
                    position[index].push(item.salary);
                }
            }
        });

        for(let index in position){
            position[index].sort(colback);
            position[index] = position[index][0];
        }

        return position;
    }
        
    countDismissed(){
        let result = {};

        this.employeeObjects.map((item) => {
            result[item.departmentName] = result[item.departmentName] || 0;
            result.length = result.length || 0;

            if(item.works === false){
                ++result[item.departmentName];
                ++result.length;
            }
        });

        return result;
    }
        
    departmentsNoManager(){
        let numbDepartament = [];
        let mass = [];
        let result = [];

        this.employeeObjects.map((item) => {
            if(item.positionNumber === 1 && item.works){
                numbDepartament.push(item.departmentNumber);
            }

            mass.push(item);
        });

        for(let index of numbDepartament){
            for(let i = 0; i < mass.length; i++){
                if(mass[i].departmentNumber === index){
                    mass.splice(i--, 1);
                }
            }
        }

        for(let index in mass){
            result.push(mass[index].departmentName);
        }

        return result = [...new Set(result)];
    }
}

let restaurant = new Restaurant(employeeObjectss);

class Statistics{
    constructor(){
        this.sumSalariesDepartmentBlock = document.querySelector('.sumSalariesDepartmentResult');
        this.averageSalaryDepartmentBlock = document.querySelector('.averageSalaryDepartmentResult');
        this.salarysDepartmentBlock = document.querySelector('.SalarysDepartmentResult');
        this.salarysPositionBlock = document.querySelector('.SalarysPositionResult');
        this.countDismissedBlock = document.querySelector('.countDismissedResult');
        this.departmentsNoManagerBlock = document.querySelector('.departmentsNoManagerResult');
    }

    sumSalariesDepartment(){
        let data = restaurant.sumSalariesDepartment();
        this.sumSalariesDepartmentBlock.innerHTML = '';
        const sumSalariesDepartmentResult = document.createElement('div');
        sumSalariesDepartmentResult.className = 'sumSalariesDepartmentResult';
        for(let index in data){
            sumSalariesDepartmentResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        return this.sumSalariesDepartmentBlock.append(sumSalariesDepartmentResult);
    }
    
    averageSalaryDepartment(){
        let data = restaurant.averageSalaryDepartment();
        this.averageSalaryDepartmentBlock.innerHTML = '';
        const averageSalaryDepartmentResult = document.createElement('div');
        averageSalaryDepartmentResult.className = 'averageSalaryDepartmentResult';

        for(let index in data){
            averageSalaryDepartmentResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        return this.averageSalaryDepartmentBlock.append(averageSalaryDepartmentResult);
    }

    salarysDepartment(data){
        data = data || restaurant.salarysDepartment((min, max) => min - max);

        this.salarysDepartmentBlock.innerHTML = '';
        const salarysDepartmentResult = document.createElement('div');
        salarysDepartmentResult.className = 'salarysDepartmentResult';

        for(let index in data){
            salarysDepartmentResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        const buttonMin = document.createElement('button');
        buttonMin.innerText = 'min';
        buttonMin.addEventListener('click', this.salarysDepartmentOnCallback.bind( this, salarysDepartmentResult, ((min, max) => min - max) ));
        
        const buttonMax = document.createElement('button');
        buttonMax.innerText = 'max';
        buttonMax.addEventListener('click', this.salarysDepartmentOnCallback.bind( this, salarysDepartmentResult, ((min, max) => max - min) ));

        salarysDepartmentResult.append(buttonMin, buttonMax);

        return this.salarysDepartmentBlock.append(salarysDepartmentResult);
    }

    salarysPosition(data){
        data = data || restaurant.salarysPosition((min, max) => min - max);

        this.salarysPositionBlock.innerHTML = '';
        const salarysPositionResult = document.createElement('div');
        salarysPositionResult.className = 'salarysPositionResult';

        for(let index in data){
            salarysPositionResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        const buttonMin = document.createElement('button');
        buttonMin.innerText = 'min';
        buttonMin.addEventListener('click', this.salarysPositionOnCallback.bind( this, salarysPositionResult, ((min, max) => min - max)));

        const buttonMax = document.createElement('button');
        buttonMax.innerText = 'max';
        buttonMax.addEventListener('click', this.salarysPositionOnCallback.bind(this, salarysPositionResult, ((min, max) => max - min)));

        salarysPositionResult.append(buttonMin, buttonMax);
        return this.salarysPositionBlock.append(salarysPositionResult);
    }

    countDismissed(){
        let data = restaurant.countDismissed();
        this.countDismissedBlock.innerHTML = '';
        const countDismissedResult = document.createElement('div');
        countDismissedResult.className = 'countDismissedResult';

        for(let index in data){
            countDismissedResult.innerHTML += `
                <p class="infoText">
                    ${index}:  ${data[index]}
                </p>
            `;
        }

        return this.countDismissedBlock.append(countDismissedResult);
    }

    departmentsNoManager(){
        let data = restaurant.departmentsNoManager();
        this.departmentsNoManagerBlock.innerHTML = '';
        const departmentsNoManagerResult = document.createElement('div');
        departmentsNoManagerResult.className = 'departmentsNoManagerResult';
        for(let index in data){
            departmentsNoManagerResult.innerHTML += `
                <p class="infoText">
                    ${data[index]}
                </p>
            `;
        }
        
        return this.departmentsNoManagerBlock.append(departmentsNoManagerResult);
    }

    salarysPositionOnCallback(param, callback){
        param.innerText = '';
        this.salarysPosition(restaurant.salarysPosition(callback));
    }

    salarysDepartmentOnCallback(param, callback){
        param.innerText = '';
        this.salarysDepartment(restaurant.salarysDepartment(callback));
    }
}

const statistics = new Statistics();

class RenderCards{
    constructor(){
        this.container = document.querySelector('.container');
        this.modal = document.querySelector('.modal');
        this.trueAdnFalse = null;
        this.renderUsers();
    }
    
    createCard(item, index){
        const buttonChange = document.createElement('button');
        const buttonDelete = document.createElement('button');
        buttonDelete.addEventListener('click', this.deleteUsers.bind(this, index));
        buttonChange.addEventListener('click', this.chengeUsers.bind(this, item, index));
        
        const card = document.createElement('div');
        card.className = 'users';
        const usersData = document.createElement('div');
        usersData.className = 'users_data';
    
        const name = document.createElement('P');
        name.innerText = `name: ${item.name}`;
        const surname = document.createElement('P');
        surname.innerText = `surname: ${item.surname}`;
        const position = document.createElement('P');
        position.innerText = `position: ${item.position}`;
        const salary = document.createElement('P');
        salary.innerText = `salary: ${item.salary}`;
        const works = document.createElement('P');
        works.innerText = `works: ${item.works}`;
        const departmentName = document.createElement('P');
        departmentName.innerText = `departmentName: ${item.departmentName}`;
    
        const usersButtons = document.createElement('div');
        usersButtons.className = 'users_buttons';
        buttonChange.innerText = 'Change';
        buttonDelete.innerText = 'Delete';
        usersButtons.append(buttonChange, buttonDelete);
    
        usersData.append(name, surname, position, salary, works, departmentName);
        card.append(usersData, usersButtons);
        
        return card;
    }

    renderUsers(){
        this.container.innerHTML = '';
        const cards = employeeObjectss.map((item, index) => this.createCard(item, index));
        this.container.append(...cards);
    }

    createModalUsers(item, index){
        this.modal.innerHTML = '';
        
        const modalChange = document.createElement('form');
        modalChange.className = 'modalChange';

        const modalChangeCap = document.createElement('P');
        modalChangeCap.className = 'modalChange_cap';
        modalChangeCap.innerText = 'User';

        const inputName = document.createElement('input');
        inputName.name = 'name';
        if(this.trueAdnFalse === true) {
            inputName.placeholder = item.name;
        }
        const name = document.createElement('span');
        name.innerText = 'name:';
        name.append(inputName);

        const inputSurname = document.createElement('input');
        if(this.trueAdnFalse === true) {
            inputSurname.placeholder = item.surname;
        }
        inputSurname.name = 'surname';
        const surname = document.createElement('span');
        surname.innerText = 'Surname:';
        surname.append(inputSurname);

        const selectPosition = document.createElement('select');
        selectPosition.name = 'position';

        const worksOption = document.createElement('option');
        if(this.trueAdnFalse === true) {
            worksOption.innerText = item.position;
        }
        const worksOptionHead = document.createElement('option');
        worksOptionHead.innerText = 'Head';
        worksOptionHead.value = 'Head';

        const worksOptionBartender = document.createElement('option');
        worksOptionBartender.innerText = 'bartender';
        worksOptionBartender.value = 'bartender';

        const worksOptiOnchef = document.createElement('option');
        worksOptiOnchef.innerText = 'chef';
        worksOptiOnchef.value = 'chef';

        const worksOptionLoader = document.createElement('option');
        worksOptionLoader.innerText = 'loader';
        worksOptionLoader.value = 'loader';

        const worksOptionSorter = document.createElement('option');
        worksOptionSorter.innerText = 'sorter';
        worksOptionSorter.value = 'sorter';

        const worksOptionWaiter = document.createElement('option');
        worksOptionWaiter.innerText = 'waiter';
        worksOptionWaiter.value = 'waiter';

        const worksOptionCook = document.createElement('option');
        worksOptionCook.innerText = 'сook';
        worksOptionCook.value = 'сook';

        const position = document.createElement('span');
        position.innerText = 'Position:';

        selectPosition.append(worksOption, worksOptionHead, worksOptionBartender, worksOptiOnchef, worksOptionLoader, worksOptionSorter, worksOptionWaiter, worksOptionCook);
        position.append(selectPosition);

        const inputSalary = document.createElement('input');
        inputSalary.type = 'number';
        inputSalary.name = 'salary';
        if(this.trueAdnFalse === true) {
            inputSalary.placeholder = item.salary;
        }
        const salary = document.createElement('span');
        salary.innerText = 'Salary:';
        salary.append(inputSalary);

        const worksSelect = document.createElement('select');
        worksSelect.name = 'works';

        const worksOptionСhoice = document.createElement('option');
        if(this.trueAdnFalse === true) {
            worksOptionСhoice.innerText = item.works;
        }
        const worksOptionTrue = document.createElement('option');
        worksOptionTrue.innerText = 'true';
        worksOptionTrue.value = true;

        const worksOptionFalse = document.createElement('option');
        worksOptionFalse.innerText = 'false';
        worksOptionFalse.value = false;
        
        const works = document.createElement('span');
        works.innerText = 'Works:';
        
        worksSelect.append(worksOptionСhoice, worksOptionTrue, worksOptionFalse);
        works.append(worksSelect);

        const selectDepartmentName = document.createElement('select');
        selectDepartmentName.name = 'departmentName';

        const optionDepartmentName = document.createElement('option');
        if(this.trueAdnFalse === true) {
            optionDepartmentName.innerText = item.departmentName;
        }
        const hallOptionDepartmentName = document.createElement('option');
        hallOptionDepartmentName.innerText = 'hall';
        hallOptionDepartmentName.value = 'hall';

        const worksOptionKitchen = document.createElement('option');
        worksOptionKitchen.innerText = 'kitchen';
        worksOptionKitchen.value = 'kitchen';

        const worksOptionWarehouse = document.createElement('option');
        worksOptionWarehouse.innerText = 'warehouse';
        worksOptionWarehouse.value = 'warehouse';

        const departmentName = document.createElement('span');
        departmentName.innerText = 'DepartmentName:';
        
        selectDepartmentName.append(optionDepartmentName, hallOptionDepartmentName, worksOptionKitchen, worksOptionWarehouse);
        departmentName.append(selectDepartmentName);

        const button = document.createElement('button');
        button.innerText = 'Add';
        button.addEventListener('click', this.chengeUserAndAddUser.bind(this, index));

        modalChange.append(modalChangeCap, name, surname, position, salary, works, departmentName, button);

        this.modal.append(modalChange);
        this.modal.addEventListener('click', this.closeModal.bind(this, this.modal));
    }

    onChengeUser(event, index){
        event.preventDefault();

        let data = new FormData(event.target.closest('form'));

        employeeObjectss[index].name = data.get('name') || employeeObjectss[index].name;
        employeeObjectss[index].surname = data.get('surname') || employeeObjectss[index].surname;
        employeeObjectss[index].position = data.get('position') || employeeObjectss[index].position;
        employeeObjectss[index].positionNumber = positionNumber[data.get('position')] || employeeObjectss[index].positionNumber;
        employeeObjectss[index].salary = Number(data.get('salary')) || employeeObjectss[index].salary;
        employeeObjectss[index].works = data.get('works') === 'true' ? true : false;
        employeeObjectss[index].departmentName = data.get('departmentName') || employeeObjectss[index].departmentName;
        employeeObjectss[index].departmentNumber = departmentNumber[data.get('departmentName')] || employeeObjectss[index].departmentNumber;

        this.renderUsers();
        this.modal.classList.remove('active');
    }

    onAddUser(event){
        event.preventDefault();
        let mass = [];
        let data = new FormData(event.target.closest('form'));
        data.get('inputName', 'inputSurname', 'selectPosition', 'inputSalary', 'worksSelect', 'selectDepartmentName');
        data.forEach((item) => {
            mass.push(item);
        });

        employeeObjectss.push({
            name: data.get('name'),
            surname: data.get('surname'),
            position: data.get('position'),
            positionNumber: positionNumber[data.get('position')],
            salary: Number(data.get('salary')),
            works: data.get('works') === 'true' ? true : false,
            departmentNumber: departmentNumber[data.get('departmentName')],
            departmentName: data.get('departmentName'),
        });

        this.renderUsers();
        this.modal.classList.remove('active');
    }

    closeModal(param, event){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }

    chengeUserAndAddUser(index, event){
        if(this.trueAdnFalse){
            this.onChengeUser(event, index);
        } else {
            this.onAddUser(event);
        }
    }

    chengeUsers(item, index){
        this.modal.classList.add('active');
        this.trueAdnFalse = true;
        this.createModalUsers(item, index);
    }

    deleteUsers(index){
        employeeObjectss.splice(index, 1);
        this.renderUsers();
    }
}

const renderCards = new RenderCards();

class Header extends RenderCards{
    constructor(){
        super();
        this.statistics = statistics;
        this.statisticsModal = document.querySelector('.statisticsModal');
        this.header = document.querySelector('.header');
        this.headerButtons();
    }
    
    headerButtons(){
        const statistics = document.createElement('button');
        const addUserButton = document.createElement('button');
        const restaurantLink = document.createElement('a');
        const bankLink = document.createElement('a');

        statistics.innerText = 'Statistics';
        addUserButton.innerText = 'Add user';
        restaurantLink.innerText = 'Restaurant';
        restaurantLink.href = './index.html';
        bankLink.innerText = 'Bank';
        bankLink.href = './bankHtml.html';
        statistics.addEventListener('click', this.activeStatistics.bind(this));
        
        this.statisticsModal.addEventListener('click', this.closeModal.bind(this, this.statisticsModal));
        addUserButton.addEventListener('click', this.addUser.bind(this));
        
        return this.header.append(statistics, addUserButton, restaurantLink, bankLink);
    }

    activeStatistics(){
        this.statisticsModal.classList.add('active');
        this.statistics.sumSalariesDepartment();
        this.statistics.averageSalaryDepartment();
        this.statistics.salarysDepartment();
        this.statistics.salarysPosition();
        this.statistics.countDismissed();
        this.statistics.departmentsNoManager();
    }

    addUser(event){
        this.modal.classList.add('active');
        this.trueAdnFalse = false;
        this.createModalUsers();
        event.stopPropagation();
    }

    closeModal(param, event){
        if (param === event.target) {
            param.classList.remove('active');
        }
    }
}

const header = new Header();
